﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace internalsorting
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int n = (int)numericUpDown1.Value;
            Record[] array = new Record[n];
            for (int i = 0; i < n; i++)
            {
                array[i] = new Record();
                array[i].key = random.Next(0, 20);
                array[i].Number = i;
            }
            Display(array, dataGridView1);
            Sort(array, 1);
            MessageBox.Show("Готово!");
        }

        private void DoEmpty(DataGridView dgv)
        {
            dgv.Rows.Clear();
            dgv.Refresh();
        }

        private void Merge(Record[] array, Record[] array1, Record[] array2, int length)
        {
            int i = 0, j1 = 0, j2 = 0;
            while (i < array.Length)
            {
                for (int k1 = 0, k2 = 0; k1 < length && k2 < length && i < array.Length; )
                {
                    if (array1[j1] != null && array2[j2] == null)
                    {
                        while (array1[j1] != null)
                        {
                            array[i] = array1[j1];
                            j1++;
                            i++;
                        }
                    }
                    else if (array1[j1] == null && array2[j2] != null)
                    {
                        while (array2[j2] != null)
                        {
                            array[i] = array2[j2];
                            j2++;
                            i++;
                        }
                    }
                    else if (array1[j1].key < array2[j2].key)
                    {
                        array[i] = array1[j1];
                        j1++;
                        i++;
                        k1++;
                    }
                    else if (array1[j1].key >= array2[j2].key)
                    {
                        array[i] = array2[j2];
                        j2++;
                        i++;
                        k2++;
                    }
                }
                Display(array, dataGridView1);
                if (array1[j1] != null && array2[j2] != null && j1 != j2)
                {
                    while (j1 < j2 && i < array.Length && array1[j1] != null)
                    {
                        array[i] = array1[j1];
                        Display(array, dataGridView1);
                        j1++;
                        i++;
                    };
                    while (j2 < j1 && i < array.Length && array2[j2] != null)
                    {
                        array[i] = array2[j2];
                        Display(array, dataGridView1);
                        j2++;
                        i++;
                    };
                }
            }
            DoEmpty(dataGridView2);
            DoEmpty(dataGridView3);
            Sort(array, length * 2);
        }

            private void Sort(Record[] array, int length)
            {
                if (length < array.Length)
                {
                    Record[] array1 = new Record[array.Length];
                    Record[] array2 = new Record[array.Length];
                    int i = 0, j1 = 0, j2 = 0;
                    while (i < array.Length)
                    {
                        for (int k = 0; k < length && i < array.Length; k++, j1++, i++)
                        {
                            array1[j1] = array[i];
                        }
                        for (int k = 0; k < length && i < array.Length; k++, j2++, i++)
                        {
                            array2[j2] = array[i];
                        }
                    }
                    Display(array1, dataGridView2);
                    Display(array2, dataGridView3);
                    Merge(array, array1, array2, length);
                }
            }

            private void Display(Record[] array, DataGridView dgv)
            {
                dgv.RowCount = 1;
                dgv.ColumnCount = array.Length;
                dgv.ClearSelection();
                int i = 0;
                while (i < array.Length && array[i] != null)
                {
                    dgv.Rows[0].Cells[i].Value = array[i].key;
                    i++;
                }
                dgv.Refresh();
                Thread.Sleep(500);
            }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
